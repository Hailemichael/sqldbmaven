package unit;

import static org.junit.Assert.assertEquals;
import static org.mule.munit.common.mocking.Attribute.attribute;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

import org.json.simple.JSONObject;
import org.junit.Test;
import org.mule.DefaultMuleMessage;
import org.mule.api.MuleContext;
import org.mule.api.MuleEvent;
import org.mule.api.MuleMessage;
import org.mule.munit.common.mocking.MessageProcessorMocker;
import org.mule.munit.runner.functional.FunctionalMunitSuite;

import common.CustomJSONParser;

public class SqlDBMavenTest extends FunctionalMunitSuite {
	
	public static final boolean DEBUG = false;

	@Override
	protected String getConfigResources() {
		return "sqldbmaven.xml";
	}
	
	 //TO Enable Inbound Endpoints
	  //@Override protected boolean haveToDisableInboundEndpoints() { return
	  //false; }
	  
	  //To enable Inbound Endpoints for specific flows
	  
	  //@Override protected List<String> getFlowsExcludedOfInboundDisabling() {
	  //return Arrays.asList("sqldbconnection.xml","more-config-xml-file.xml"); }
	  
	  //To disable Mocking of connectors
	  //@Override protected boolean haveToMockMuleConnectors() { return false; }
	 
	
	//@Override
	protected void muleContextStarted(MuleContext muleContext) {
		System.out.println("Mule Context Started at: " + 
				new Date(muleContext.getStartDate()));
	}
	
	
	@SuppressWarnings("unchecked")
	@Test
	public void testGetEmployeeById() throws Exception {

		// Prepare the mocked SQL connector return message
		ArrayList<LinkedHashMap<String, Object>> mockedSQLDBResult = new ArrayList<LinkedHashMap<String, Object>>();
		LinkedHashMap<String, Object> map = new LinkedHashMap<String, Object>();
		map.put("ID", 1);
		map.put("name", "testName");
		map.put("company", "testCompany");
		map.put("jobTitle", "testJobTitle");
		mockedSQLDBResult.add(map);
		MuleMessage mockedSQLDBMessage = muleMessageWithPayload(mockedSQLDBResult);

		// Mock the SQL DB Connector
		MessageProcessorMocker mocker = whenMessageProcessor("select")
										.ofNamespace("db")
										.withAttributes(attribute("name")
										.ofNamespace("doc")
										.withValue("Database"));
		mocker.thenReturn(mockedSQLDBMessage);

		// Set input message to the flow to be tested
		Object payload = null;
		final MuleMessage muleMessage = new DefaultMuleMessage(payload, muleContext);

		// Create Mule Event that handle the input message
		MuleEvent requestEvent = testEvent(null);
		requestEvent.setMessage(muleMessage);
		MuleEvent event = runFlow("sqldbmavenFlow", requestEvent);

		// Get the actual Employee data from the received message
		MuleMessage receivedMessage = event.getMessage();
		if (DEBUG)
			System.out.println("Recieved Message: " + receivedMessage);
		
		Map<String, Object> actualMap = CustomJSONParser.getMapForValidJSONString(receivedMessage.getPayloadAsString("ISO-8859-1"));
		JSONObject actualJSONObject = new JSONObject();
		if (actualMap != null) {
		actualJSONObject.putAll(actualMap);
		}
		if (DEBUG)
			System.out.println("Actual JSON Object" + actualJSONObject);

		// Prepare the expected result
		JSONObject expectedJSONObject = new JSONObject();
		expectedJSONObject.putAll(map);
		assertEquals(expectedJSONObject.toString(), actualJSONObject.toString());
		if (DEBUG)
			System.out.println("Expected JSON Object" + expectedJSONObject);
	}

}

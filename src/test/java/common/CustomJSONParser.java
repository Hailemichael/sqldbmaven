package common;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.json.simple.parser.ContainerFactory;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class CustomJSONParser {
	private static JSONParser parser = new JSONParser();

	/*
	 * Used to return a properly ordered Map or list of Arrays
	 */
	private static ContainerFactory getContainerFactory() {
		return new ContainerFactory() {
			public List<Map<String, Object>> creatArrayContainer() {
				return new ArrayList<Map<String, Object>>();
			}

			public Map<String, Object> createObjectContainer() {
				return new LinkedHashMap<String, Object>();
			}
		};
	}

	/*
	 * Verifies if a given string is a JSON string and parse it to a Map
	 * 
	 * @param jsonString the String to be parsed
	 * 
	 * @return a properly ordered map from the JSON string
	 */
	@SuppressWarnings("unchecked")
	public static LinkedHashMap<String, Object> getMapForValidJSONString(String jsonString) {
		LinkedHashMap<String, Object> map = new LinkedHashMap<String, Object>();
		try {
			map = (LinkedHashMap<String, Object>) parser.parse(jsonString, getContainerFactory());
		} catch (ParseException pe) {
			return null;
		}
		return map;
	}
}

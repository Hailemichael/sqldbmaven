package integration;

import static org.junit.Assert.assertEquals;

import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.json.simple.JSONObject;
import org.junit.Test;
import org.mule.DefaultMuleMessage;
import org.mule.api.MuleMessage;
import org.mule.api.registry.RegistrationException;
import org.mule.api.registry.Registry;
import org.mule.api.transport.PropertyScope;
import org.mule.module.client.MuleClient;
import org.mule.tck.junit4.FunctionalTestCase;
import org.springframework.beans.factory.annotation.AutowiredAnnotationBeanPostProcessor;
import org.springframework.beans.factory.annotation.Value;

import common.CustomJSONParser;

public class SqlDBMavenITCase extends FunctionalTestCase {

	public static final boolean DEBUG = true;
	
	@Value("${http.host}")
    protected String host;
	
	@Value("${http.port}")
    protected String port;
	
	@Override
	protected String getConfigResources() {
		return "sqldbmaven.xml";
	}
	
	@Override
    protected void doSetUp() throws RegistrationException {
        if (muleContext.isStarted()) {
            final Registry registry = muleContext.getRegistry();
            final AutowiredAnnotationBeanPostProcessor processor;
            try {
            	processor =
                		registry.lookupObject(AutowiredAnnotationBeanPostProcessor.class);
            } catch (final RegistrationException theException) {
                logger.error("An error occurred retrieving AutowiredAnnotationBeanPostProcessor", theException);
                throw theException;
            }
 
            if (processor != null) {
            	processor.processInjection(this);
            } else {
                logger.warn("No AutowiredAnnotationBeanPostProcessor in the Mule registry, "
                    + "could not perform dependency injection on the test instance.");
            }
        } else {
            logger.warn("Mule context is not started, no dependency injection on test instance performed.");
        }
    }
	
	@SuppressWarnings("unchecked")
	@Test
	public void integrationTestGetEmployeeById() throws Exception {
		
        //Set input message payload
        Object payload = null; 
        
        //prepare input message properties
        final MuleMessage muleMessage = new DefaultMuleMessage(payload, muleContext);
        muleMessage.setProperty("http.method", "GET", PropertyScope.INVOCATION);
                
        //Create Mule Client and send request with input message
        MuleClient muleClient = new MuleClient(muleContext);
	    MuleMessage receivedMessage = muleClient.send("http://" + host + ":" + port + "/sqldbmaven/1", muleMessage);
        if(DEBUG) System.out.println("Recieved Message: " + receivedMessage);
	    
        Map<String, Object> actualMap = CustomJSONParser.getMapForValidJSONString(receivedMessage.getPayloadAsString("ISO-8859-1"));
		JSONObject actualJSONObject = new JSONObject();
		if (actualMap != null) {
		actualJSONObject.putAll(actualMap);
		}
		
	    if(DEBUG) System.out.println("Actual JSON Object" + actualJSONObject);
	    
	    //Prepare the expected result
		LinkedHashMap<String, Object> map = new LinkedHashMap<String, Object>();	        
		    map.put("ID", 1);
		    map.put("name", "testName");
		    map.put("company", "testCompany");
		    map.put("jobTitle", "testJobTitle");		
	    JSONObject expectedJSONObject = new JSONObject();
	    expectedJSONObject.putAll(map);
	    if(DEBUG) System.out.println("Expected JSON Object" + expectedJSONObject);
	    
	    assertEquals(expectedJSONObject.toString(), actualJSONObject.toString()); 	    	 
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void integrationTestGetEmployeeByIdHTTPClient() throws Exception {
		
        //Set input message payload
        Object payload = null; 
        
        //prepare input message properties
        final MuleMessage muleMessage = new DefaultMuleMessage(payload, muleContext);
        muleMessage.setProperty("http.method", "GET", PropertyScope.INVOCATION);
                

	    //Prepare HTTP CLient
	    HttpClient client = HttpClientBuilder.create().build();
	    HttpGet request = new HttpGet("http://" + host + ":" + port + "/sqldbmaven/1");
	    
	    // add request header
	    request.addHeader("http.method", "GET");
	    request.addHeader("Accept", "application/json");	
	    
	    //Response
	    HttpResponse response = client.execute(request);
	    String receivedPayload = EntityUtils.toString(response.getEntity(), "ISO-8859-1");
	    
        if(DEBUG) System.out.println("Recieved Payload: " + receivedPayload);
	    
        Map<String, Object> actualMap = CustomJSONParser.getMapForValidJSONString(receivedPayload);
		JSONObject actualJSONObject = new JSONObject();
		if (actualMap != null) {
		actualJSONObject.putAll(actualMap);
		}
		
	    if(DEBUG) System.out.println("Actual JSON Object" + actualJSONObject);
	    
	    //Prepare the expected result
		LinkedHashMap<String, Object> map = new LinkedHashMap<String, Object>();	        
		    map.put("ID", 1);
		    map.put("name", "testName");
		    map.put("company", "testCompany");
		    map.put("jobTitle", "testJobTitle");		
	    JSONObject expectedJSONObject = new JSONObject();
	    expectedJSONObject.putAll(map);
	    if(DEBUG) System.out.println("Expected JSON Object" + expectedJSONObject);
	    
	    assertEquals(expectedJSONObject.toString(), actualJSONObject.toString()); 	    	 
	}
	
	
}
